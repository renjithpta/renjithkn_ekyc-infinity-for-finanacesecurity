![Heading](/uploads/079a568b1ae9570d0adecc4de3fb80eb/Heading.PNG)
![underline](/uploads/5a121d6aee41e4776f23171a063411ac/underline.PNG)

Most financial institutions are under considerable pressure to deliver safer but faster customer onboarding journeys. The key to achieving this comes down to their ability  to manage client lifecycle events and milestones via re-usable, verified, and up-to-date customer information.

The core of what takes onboarding so long is the process of collecting, validating, and  processing customer data to achieve the right compliance decision. In today’s ‘digital-first’ world, business’ subject to know your customer (KYC) requirements must find a way to create better onboarding and authentication procedures that achieve compliance and combat fraud, without alienating good customers.
It is crucial that financial institutions can access the right data at the right time. Reusability and transparency are essential for this . If a client onboarding team can easily access the customer information needed to complete KYC instantaneously, the result will be a much faster onboarding process and better client experience.


I am  using ‘Blockchain Technology’ to help combat the issues of very long wait times,unverified client identities, and an unreasonably repetitive process for each institution when selling securities. I have leveraged Hyperledger Fabric to create a consortium solution for various financial institutions, resulting in a modernized interaction structure. I have created an interface for clients who want to buy securities from a financial institution to go through a one-time verification process with the institution, thereafter, not having to go the verification
process again if they decide to buy securities from a different institution. When a client has been verified by a financial institution, other financial institutions will be able to retrieve verified client information instantaneously. My solution also considers authorization implications by creating a process for the client to give only certain institutions access to certain information, protecting them from having institutions accessing their information without approval. Privatedate collection is used to keep client rating and sharing it only between the requested organizations.

# **Solution**
- Using g ‘Blockchain Technology’ to help combat the issues of very long wait times, unverified client identities, and an unreasonably repetitive process for each institution when selling securities. 

- Leverage  Hyperledger Fabric to create a consortium solution for various financial institutions, resulting in a modernized interaction structure.
 
- Also create an interface for clients who want to buy securities from a financial institution to go through a one-time verification process with the institution, thereafter, not having to go the verification process again if they decide to buy securities from a different institution. 

- When a client has been verified by a financial institution, other financial institutions will be able to retrieve verified client information instantaneously.

- The  solution also considers authorization implications by creating a process for the client to give only certain institutions access to certain information, protecting them from having institutions accessing their information without approval.


The Electronic Know Your Client (eKYC) is a layer for digital identity that allows a web application on which the seller can look up if the buyer meets clear requirements before the transaction, as well as register the buyer through (eKYC). The sellers need reliable and independent source data or information.  Also, buyers can have a relationship without an intervening authority – self-sovereign.   Nowadays, the lack of security with a person's identity exists in each application that users store their information and give control to central authorities to manage them.

![Flow](/uploads/3b464d43f0b051e1309e69d1959ed6f1/Flow.PNG)








7.Run make start-api.

![startapi](/uploads/eef5e800bc5a7c0ecfd7374debdd57a7/startapi.gif)




# Why fabric


- Blockchain is applicable for digital identity because it is immutable, legitimate, and validated by multiple participants in the networking. This architecture prevents Anti-Money Laundering (AML) and protects sellers of the securities aware of the financial status of the buyer and prevents fraudulent behavior, as well as client information. Unique set of advantages using a decentralized distributed ledger enabling seamless secure sharing of information.
 
- The application uses a permissioned system based on roles. Furthermore, uses Hyperledger Fabric to control transactions, approve transactions and establish new rules.


- Using Fabric enables the same system can be used for other decentralized sharing of joint ventures, easy takeover etc.

- Fabric is stable  and made for general-purpose. Also, Fabric is not accessible anonymously. 



# **How it works**


- Each organization in the ledger represents a financial institution (FI) and has an enrolled user in order to access the system.

- Only FIs are able to perform submit or evaluate transactions and also register new clients that want to keep their information in the system.

- Once a new client is registered, the FI who registered that client is automatically able to access the client data and that client cannot remove access to this institution.

- Clients are able to approve and remove approval from other registered institutions to access their data.  

- Considering that only FIs can perform transactions, in order to approve or remove approval clients must have organization number and ledger user information. For such, these information are encrypted and stored in the database. Once the request is made, thebackend decrypts that information to perform the transaction.

- A composite key is employed to map the approvals. We have clientId~fiId and fiId~clientId composite keys to map this relationship.

**Work flow & Functionality**

The blockchain would enable:
- Storing KYC information on a user
- Putting out a request for the KYC of a user
- Responding to the request
- Response using only necessary information
- If there are no responses, then get the data from the user.
- Rate the customer and share between member orgs as private data

![flowenchance](/uploads/c27ed6830e2bfa7c9896cfa903efad71/flowenchance.PNG)

_Web interface_

![WebInterface](/uploads/a27d498d1cb3d434fc5db6af5e837a20/WebInterface.PNG)


**Participant /Roles & State Data**

**Roles /Participants**
    System    – eKYC Infinite solution.
    Seller    – Financial Institutions selling securities
    Buyer     – Clients who want to buy securities
Ledger data


**Client**
    Finname: Name of the client
    dateOfBirth: Date of birth of the client
    address: Address of the client
    idNumber: Some identification number of the client
    whoRegistered: { orgNum: number of the organization that registered the client,
    ledgerUser: name of the ledger user that registered the client}


   
**Financial Institution**
    name: Name of the financial institution
    idNumber: Some identification number of the financial institution
    orgCredentials: { orgNum: number of the organization, ledgerUser: name of the
    ledger user of that organization}



**Running the Apllication**

 Install Node Js .V8.X.x, VS code with IBM Blockchain platform , Ansible latest, python 3.7 , Mongo DB and make. 

1. Run the Fabric FI network.
   
     1. Make sure that make is installed and clone the repo.
     2. Go root of the cloned  folder and run make start.





     



     ![startFI](/uploads/a8942a7f4f5b3488409dae8588d4c580/startFI.gif)



     ![screen-capture2](/uploads/8300a1e3960952c98a99c086317d9fbd/screen-capture2.gif)



     ![part3](/uploads/4ec9acd784a52575e35c8dad6b7202e0/part3.gif)



     ![part4](/uploads/7c6c0411cc078d1d8594ad0b73a09e91/part4.gif)



     3 .Use IBM VS code extension to install the app .

     
     4.  Selcet privatedata option yes when deploying and browse collection.json inside the chaincode folder.
     







     ![ezgif.com-gif-maker](/uploads/a557d21f238d7106d64f990773ebc617/ezgif.com-gif-maker.gif)

      


      ![installintantiate](/uploads/3cea9c90bf72967e6a511e71adfb5c92/installintantiate.gif)




      ![instantiate](/uploads/f4a2338481f1ea40f955c286960ffe5b/instantiate.gif)



      ![intantiatepart3](/uploads/7f16676ff38193f5039dddb497a4bff4/intantiatepart3.gif)
      ![finalinstantiate](/uploads/8ec1163443c20045e4d2466168400d2e/finalinstantiate.gif)



    

     Now Ntwork is up and chaincode installed. The next step is up the api server the smaple client data migrated to blockchain and enoll user credtial for frontend access in mongodb with username and password.

     4 . Run make install-api for installing all the node modules.

     5. Run make initalize-api for initalizing blockchain with smaple client and enroll admin/user for each            organization . The gateway folder under ApiandFrontend/api will contain the wallets of the all user enrolled fro each FI orgs. Also, mongodb will be populated with use login details .

     ![initapifirts](/uploads/209e2f98e0ff8b9824bdcfdc7500684f/initapifirts.gif)


       
    ![initapipart2](/uploads/bd53f696aba6bc05886ea17c25d056b5/initapipart2.gif)


    ![initpar3](/uploads/a2e4aedfc82c7364a5d00f3db6c1c9ec/initpar3.gif)



    7. Run make start-api.

    ![startapi](/uploads/eef5e800bc5a7c0ecfd7374debdd57a7/startapi.gif)







    8. Run make install-frontend



    ![start-api](/uploads/a09451c3e353c1db52f4479ae4717a55/start-api.gif)



    9.Run make run-frontend


    
    ![rundrontend](/uploads/5eefeca39e912dfe281b83948e4584c0/rundrontend.gif)





    ![runfronend2](/uploads/312d00f1fa3e10ad10d44c6a34944725/runfronend2.gif)


**Working video demo https://youtu.be/_wILRFZN0dQ **
.

#### Login credentials

Client
* login: user01 / user02 / JonasKahnwald / MarthaNielsen / ClaudiaTiedemann / ElisabethDoppler / H.G.Tannhaus
* password: 123456

Financial Institution
* login: FI1 / FI2
* password: 123456


# Endpoints




##  /client/create


- method: POST
- params: login, password, name, date of birth, address, id number
- description: register new client
- chaincode: calls 'createClient' function

## **/client/login**
- method: POST
- params: login, password, user type
- description: client login

    
## **/client/getClientData**

- method: GET
-   params: ord number, ledger user, ledge ID
-   description: return the data from the client
-   chaincode: calls 'getClientData' function

/client/approve
  method: POST
  params: org number, ledger user, ledger ID, financial institution ID
  descriptiion: approve a financial institution to read client data
 chaincode: calls 'approve' function
 /client/remove
 method: POST
 params: org number, ledger user, ledger ID, financial institution ID
- description: remove a financial institution to read client data
- chaincode: calls 'remove' function
## /client/getApprovedFis
- meth-d: GET
- params: org number, ledger user, ledger ID
- description: return a list of approved financial institutions by the client
- chaincode: calls 'getRelationByClient'
## /fi/create
- method: POST
- params: login, password, name, idnumber
- description: register new financial institution
- chaincode: calls 'createFinancialInstitution'
## /fi/createClient
- method: POST
- params: login, password, name, date of birth, address id number, org number,
ledger user
- description: register new client
- chaincode: call createClient¡¦
## /fi/login

- method: POST
- params: login, password, usertype
- description: financial institution login

## /fi/getFiData
- method: GET
- params: org number, ledger user
- description: return the data from the financial institution
- chainc-de: calls 'getFinancialInstitutionData' function

## /fi/getClientData
- method: GET
- params: org number, ledger user, client ID, fields wanted
- description: return the data from the client
- chaincode: calls 'getClientData' function


## /fi/getApprovedClients
- method: GET
- params: org number, ledger user
- description: return a list of clients that approved this financial institution
- chaincode: calls 'getRelationByFi'


## **Ledger data**



## **Client**


- name: Name of the client
- dateofBirth: Date of birth of the client
- address: Address of the client
- email- email address of the client
- idNumber: Some identification number of the client
- whoRegistered: { orgNum: number of the organization that registered the client,
                   ledgerUser: name of the ledger user that registered the client}


## Financial Institution
- name: Name of the financial institution
- idNumber: Some identificati-n number of the financial instituti-n
- OrgCredentials: { OrgNum: number of the organization, ledgerUser: name of the
ledger user of that organization}


## Chaincode functions
- initLedger(ctx)
 Initiate ledger storing financial institution and client data
- getCallerId(ctx)
 Internal function  return the ledger user that called the method
- isWhoRegistered(ctx, clientId)
 Internal function  return the ledger user the registered that client
- createClient(ctx, clientData)
 Create a new client. Who registers the client is who called the method
- getClientData(ctx, clientId, fields)
 Return client data. A list -f fields wanted is passed as a parameter
- getFinancialInstitutionData(ctx)
 Return financial institution data
- approve(ctx, clientId, fiId)
 Approve a financial institution to access client data
- remove(ctx, clientId, fiId) Remove financial instituti-n access data approval

## Remove financial instituti-n access data approval
- getRelatonsArray(ctx, relationResultsIterator) Internal functi-n  iterate a c-mp-site key iterator
- getRelationByClient(ctx, clientId) Return a list of approved financial institution by that client
- getRelationByFi(ctx) Return a list of clients who approved the caller FI
- queryAllData(ctx) Return a list of all data stored in the ledger
## Approvals
- Composite key clientId~fiId
 Maps all FIs approved by the client
- Composite key fiId~clientId
 Maps all clients that approved that FI


##  Governance

    The ‘eKYC Infinite’ platform helps to store KYC data in a Distributed Ledger, where data
    can be accessed by financial institutions with authorization. It helps to improve data
    governance and as result the organization/consortium manages KYC compliance swiftly and
    efficiently.


**Membership Service Providers(MSP)**
‘eKYC Infinite’ will act as the MSP and the Blockchain network is governed by
‘eKYC Infinite’ for all the organizations using eKYC service. MSP will perform onboarding
process and will generate X.509 certificates to encrypt the digital identities of all the blockchain
members in the network

![fabricKYCnetwork](/uploads/13f20f2c6c1b163c34aa98b9efef30cb/fabricKYCnetwork.PNG)


**Endorsing Peers**

Financial Institutions will act as Endorser peers. Endorsing peers verify signature and
execute transactions. Financial institutions will scan through the Distributed Ledger to check if
the client data already exists, if not, this financial institution will update client KYC information
into the ledger, and it will be the responsibility of the endorsing peers to ensure submitted
information satisfies the policy of the blockchain network.

**Ordering service nodes**

FI1 Orderer will act as the Ordering service nodes. Ordering services assure
deterministic features the consensus algorithms, which means any block validated by the peer is
guaranteed to be final and correct. Orderers also maintain the list of organizations that are
allowed to create channels.

**Channels**
‘eKYC infinite only has one channel which stores client data in key/value pairs. Only
approved members can access the ledger.

**PrivateData collection**
 Used to store and share individual customer rating as buyer 

**Client Application**
‘eKYC infinite’ stores clients' private key in a custodial wallet. It is the responsibility
of eKYC infinite to ensure security of the client’s private keys during the duration of their
membership.









## :book: Resources and technologies :computer:

1. Chaincode

   - [IBM blockchain platform wih ansible](https://github.com/IBM/Create-BlockchainNetwork-IBPV20) - get started network  for Hyperledger Fabric
   - [Fabric contract API](https://www.npmjs.com/package/fabric-contract-api) - contract interface to implement smart contracts
   - [TypeScript](https://github.com/microsoft/TypeScript) - Chaincode language
   - [ESlint](https://eslint.org/) - pluggable JS linter

2. API

   - [Express.js](http://expressjs.com/) - web application framework
   - [MongoDB](https://www.mongodb.com/) - NoSQL database
   - [Mongoose](https://mongoosejs.com/) - object data modeling (ODM) library for MongoDB and Node.js
   - [Async](https://caolan.github.io/async/v3/) - library to perform asynchronous operations
   - [Express validator](https://express-validator.github.io/docs/) - middleware to validate data
   - [Bcryptjs](https://www.npmjs.com/package/bcryptjs) - library to perform cryptography
   - [JWT. IO](https://jwt.io/) - JSON Web Tokens to allow, decode, verify and generate JWT
   - [Dotenv](https://www.npmjs.com/package/dotenv) - loads environment variables from a .env file
   - [Fabric CA Client](https://www.npmjs.com/package/fabric-ca-client) - SDK for Node.js to interact with HLF CA
   - [Fabric Network](https://www.npmjs.com/package/fabric-network) - SDK for Node.js to interact with HLF

3. Frontend

   - [Rimble](https://rimble.consensys.design/) - design system
   - [ReactJS](https://reactjs.org/) - frontend library
   - [React router dom](https://www.npmjs.com/package/react-router-dom) - routing and navigation for react apps
   - [React-cookie](https://www.npmjs.com/package/react-cookie) - cookie interaction for React applications
   - [Axios](https://www.npmjs.com/package/axios) - HTTP requests
